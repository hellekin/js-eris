# js-eris

ERIS encoder and decoder for JavaScript.

## Roadmap / TODOs

### 1.0.0 release

- [ ] finalise ERIS encoding scheme
- [ ] Encode and decode from and to a stream
- [ ] Verification capability
- [ ] Random access to content

## Examples

See the [examples](./examples) folder.

